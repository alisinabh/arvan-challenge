# Arvan-Challenge

Alisina Bahadori (alisina.bm@gmail.com)

## Issue

Build a system to store and process `access_log` from `nginx` using `apache kafka` for stream processing and `clickhouse` for storage and analytical queries.

The system is preferred to run on `kubernetes` and `clickhouse` is expected to be deployed in a replicated and distributed manner with `shards=2` and `replication=2`.

## Overview

```
 -------       -----------------      ----------      -------      ------------
| Nginx |  => | access.log File | => | kafkacat | => | kafka | => | clickhouse |
 -------       -----------------      ----------      -------      ------------
```

 - Deploy `kafka`.
 - Deploy `clickhouse`.
 - Stream `access_log` from `nginx` pod to `kafka` and mutate logs.
 - Configure `clickhouse` to stream from `kafka` topic to another view.
 - Test that logs will get populated into `clickhouse`.
 - Conclusion (and compare with ELK).

## Steps

### Deploy Kafka

In order to deploy a kafka cluster on kubernetes, First of all we need a `zookeeper` cluster for kafka discovery. Sure we can write the manifests for deployment of all things that we need, But we can use helm charts to deploy them easily and focus on other problems that we have.

Helm chart from `bitnami/kafka` was my choice and the default values were used for sake of demonstration. Kafka here was deployed in a single instance but scaling would be as easy as just increasing the replicas parameter.

### Deploy clickhouse

To deploy clickhouse I was unable to find a reliable and well documented helm chart.
But while I was searching I found a operator for clickhouse ([clickhouse-operator](https://github.com/Altinity/clickhouse-operator)) which helps with deployment and management of clickhouse clusters running on kubernetes.

Installing the operator is straightforward. And in order to deploy our cluster we would need to create a new `ClickHouseInstallation` manifest which is pretty easy to do for simple clusters.

```yaml
apiVersion: "clickhouse.altinity.com/v1"
kind: "ClickHouseInstallation"
metadata:
  name: "arvn-challenge-1"
spec:
  configuration:
    clusters:
      - name: "arvn-shard2-repl2"
        layout:
          shardsCount: 2
          replicasCount: 2
```

Then we will just apply this in our namespace of choice and after a while our cluster gets initiated.

### Streaming access_log

Our next problem is how to stream nginx access logs to kafka in a good format.

The first thing we need is a good format for our logs in nginx. The default `main` access log format is no good since it is meant more for humans than machines. Of course we can use a proxy stream in kafka and parse those log messages but it is more efficient to generate formatted logs.

Clickhouse knows json and can read json from it's kafka engine. So I aimed for a json formatted log in nginx which surprisingly was easy. Just wrap it in a json string.

```nginx
log_format  main  '{"ip": "$remote_addr","time": "$time_iso8602","request": "$request",
					"status": $status,"response_size": $body_bytes_sent,
					"referrer": "$http_referer","ua": "$http_user_agent",
					"forwarded_for": "$http_x_forwarded_for"}';
```

So you might say:

> Will it be a safe json? What would happen if someone added a double quote or newline in their user-agent header?

Well newline wont be possible because of the structure of HTTP. And double quotes will be handed over as `%20` so yes. This would be safe.

To mount this config in our nginx container we will push it in a configMap in kubernetes and for instance mount it as `/etc/nginx/nginx.conf`.

Ok now we have our structured logs. How are we going to stream these logs into kafka in real time?

One solution is just write logs to SDTOUT and use something like `fluentbit` to stream logs to kafka but that is a bit overkill. Also other logs may be printed to STDOUT and that will make this process a bit clumsy.

**kafkacat** is a generic non-jvm kafka producer and consumer. It's written in C and is very tiny.
We can start streaming nginx access.log file to kafka with a simple command:

```sh
tail -F /var/log/nginx/access.log | kafkacat -P -b kafka -t nginx
```

Great! Now how are we going to ship kafkacat with nginx.

We have two ways. First one would be to create a new docker image from nginx image and add kafkacat binary to that. But this adds managing a new image to our headaches. More importantly the entrypoint would be a bit complicated. 

Since we are in kubernetes we can use **sidecars**. Sidecars are just auxiliary containers attached to our main container and they can share volumes. So we can add a container to our nginx pod and share the access.log file through an `emptyDir` volume. This way we are not touching our nginx container.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-app
  # ...
spec:
  # ...
  template:
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 80
          volumeMounts:
            - name: shared-data # Shared data between containers
              mountPath: /var/log/nginx/
            - name: config # Configuration for nginx logging is here
              mountPath: /etc/nginx/nginx.conf
              subPath: nginx.conf
        - name: kafkacat
          image: confluentinc/cp-kafkacat
          volumeMounts:
            - name: shared-data # Shared data between containers
              mountPath: /var/log/nginx/
          command: ["sh"]
          args: ["-c", "tail -F /var/log/nginx/access.log | kafkacat -P -b kafka -t nginx"]
      volumes:
        - name: shared-data
          emptyDir: {} # We wont need persistance for logs. Just sharing.
        - name: config
          configMap:
            name: nginx-conf
```

After deploying our `nginx-app` deployment we can send some arbitrary requests to nginx and then verify that kafka is receiving our logs by using `kafka-topics.sh --list` command. If it shows `nginx` then we are go!

### Configure clickhouse consumer

So we are successfully sending logs to kafka. We have a producer and now we need to configure our consumer. clickhouse has a kafka engine for tables. It simply connects to kafka and whenever queried, reads the data. It can be created with the following command.

```sql
CREATE TABLE kafka ( time DateTime, request String, ua String, status UInt32, 
					 response_size UInt32, referrer String, ua String, 
					 forwarded_for String, ip String ) 
					 ENGINE = Kafka('kafka.default.svc.cluster.local:9092', 
									'nginx', 
									'clickhouse1', 
									'JSONEachRow');
```

Now if we run `SELECT * FROM kafka` It will show us our logs. But if we run it again, Nothing is printed and the table is empty. That is because this table is just a kafka consumer **on reads** and does not persist any data. In order to persist data we need to create a new table and read from kafka table to our new table with a persistent engine like MergeTree.

```sql
CREATE TABLE view ( time DateTime, request String, ua String, status UInt32, 
					response_size UInt32, referrer String, ua String, 
					forwarded_for String, ip String ) 
					ENGINE = MergeTree() ORDER BY time;


CREATE MATERIALIZED VIEW consumer TO view AS
SELECT * FROM kafka;
```

### Testing

And now if we send some more requests to nginx we can verify our logs are getting stored in clickhouse by running `SELECT * FROM view`. You can see this time our logs are getting stored in the table by running this command again.


### Conclusion

Clickhouse is an OLAP datastore. It does not store tables like OLTP datatores. Simply it knows more information about columns than rows. It is more efficient for immutable data with huge reads for analytical purposes for instance building something like Google analytics. It is a columnar database management system and nothing is stored with the values. It also supports data compression.

ELK stack is good but not meant for pure analytical purposes and more for search. If we need to provide insight about our data, Such as hit rate based on URL clickhouse will be much more efficient but if for instance we are looking for a special parameter in our logs clickhouse won't be a good choice.

Kafka on the other hand helps us to offload our writes and build a queue system which helps with handling load spikes since writing in clickhouse is not very fast and actually writing in batches is preferred.
